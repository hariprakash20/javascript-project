let count =0;
let operend1 =0;
let currentOp ="";

function buttonClick(value) { 
    valueType(value);
    console.log(count);
    updateDisplay();
}

function valueType(value){
    if(value == 'C')
    count=0;
    else if(value =='=')
    evaluate();
    else if(value =='←')
    backspace();
    else if(isNaN(parseInt(value)))
    operator(value);
    else
    appendNumber(value);

}

function appendNumber(value){
    if (count==0)
    count = parseInt(value);
    else 
    count = parseInt(count+value);
}

function backspace(){
    if (count!=0)
    count = Math.floor(count/10);
}

function operator(value){
    if (currentOp==""){
        operend1 =count;
        count=0;
        currentOp = value;
    }
    else{
        evaluate();
        operend1 = count;
        count=0;
        currentOp = value;

    }

}

function evaluate(){
    if(operend1!=0){
        switch (currentOp)
        {
            case '+':
                count= operend1+count;
                currentOp ="";
                break;
            case '-':
                count = operend1-count;
                currentOp ="";
                break;
            case '×':
                count = operend1*count;
                currentOp ="";
                break;
            case '÷':
                count = operend1/count;
                currentOp ="";
                break;
        }
    }
}

function updateDisplay(){
    let disp =document.querySelector('.display');
    disp.innerHTML=count;
}
function init(){
    document.querySelector('.keys').addEventListener('click',function(event){
        buttonClick(event.target.innerText) ;
        });
}

init();